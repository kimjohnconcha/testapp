class CreateNotes < ActiveRecord::Migration[5.2]
  def change
    create_table :notes do |t|
      t.string :image
      t.integer :user_id
      t.string :title
      t.string :description
      
      t.timestamps
    end
  end
end
