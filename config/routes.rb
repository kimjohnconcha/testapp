Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  namespace :api ,path: '' do
    namespace :v1 do
      devise_scope :user do
        post 'sign-up', to: 'users/registrations#create'
        post 'sign-in', to: 'users/sessions#create'
      end

      resources :notes
    end
  end

end
