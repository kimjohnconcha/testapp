class ApplicationController < ActionController::Base
   def check_authentication
      @user = User.find_by_token(params[:token])
      if @user.present?
        return @user
      else
        render json: { error: "Invalid user authentication token", status: 300 }, status: 200
      end   
   end
end
