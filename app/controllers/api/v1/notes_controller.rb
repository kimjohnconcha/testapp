class Api::V1::NotesController < ApplicationController
   skip_before_action :verify_authenticity_token
   before_action :check_authentication

   def create
      if params[:note][:id].present?
         note = @user.notes.find(params[:note][:id])
         if note.present?
            note.update_attributes(note_params)
            render json: { note: note, status: 200 }, status: 200
         else
            render json: { error: "Update error", status: 300 }, status: 200
         end
      else
         note = @user.notes.new(note_params)
         if note.save
             render json: { note: note, status: 200 }, status: 200
         else
             render json: { error: "Unable to create a note", status: 300 }, status: 200
         end
      end
   end

   def index
      notes = Note.where("user_id = ?", @user.id)
      render json: {success: 1, notes: notes, status: 200 }, status: 200
   end

   # def update
   #    if params[:id].present?
   #       note = @user.notes.find(params[:id])
   #       if note.present?
   #          note.update_attributes(note_params)
   #          return render json: { success: 1, message: "Successfully updates a note.", status: 200 }, status: 200
   #       else
   #          render json: { error: "Update error", status: 300 }, status: 200
   #       end
   #    else
   #       render json: { error: "Update error", status: 300 }, status: 200
   #    end
   # end

   def destroy
      if params[:id].present?
          note = @user.notes.find(params[:id])
          if note.present?
            note.destroy
            return render json: { success: 1, message: "Successfully deleted a note.", status: 200 }, status: 200
          else
            render json: { error: "Unable to delete a note", status: 300 }, status: 200
          end
      else
         render json: { error: "Unable to delete a note", status: 300 }, status: 200
      end
  end



   private

   def note_params
      params.require(:note).permit(:id, :title, :description, :image,)
   end




end