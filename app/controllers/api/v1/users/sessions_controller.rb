# frozen_string_literal: true

class Api::V1::Users::SessionsController < Devise::SessionsController
  skip_before_action :verify_authenticity_token
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  def create
    @user = User.find_by_email(user_params[:email])
    if @user.present?
      if @user.valid_password?(user_params[:password])
        sign_in @user
        @user.generate_auth_token
        user = { id: @user.id, email: @user.email, username: @user.username, token: @user.token}
        render json: { user: user, status: 200 }, status: 200
        sign_out @user
      else
        render json: { error: "Invalid email or/and password", status: 300 }, status: 200
      end
    else
      render json: { error: "Email is not registered!", status: 300 }, status: 200
    end
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end

  private

  def user_params
    params.require(:user).permit(:email, :password)
  end

end
