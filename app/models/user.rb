class User < ApplicationRecord
  validates :email, presence: true
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable
  after_create :generate_auth_token

  has_many :notes


  def generate_auth_token
    secret = Digest::SHA1.hexdigest(SecureRandom.urlsafe_base64)
    self.token = User.find_by_token(secret.to_s) ? generate_auth_token : secret
    self.save
  end
end
